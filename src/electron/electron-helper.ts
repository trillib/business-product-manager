export class ElectronHelper {
  static isElectron() {
    return window && window.process && window.process.type;
  }
}
