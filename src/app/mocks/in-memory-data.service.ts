import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {

  constructor() {}

  createDb() {
    const products = [
      { id: 1, name: 'Brot', price: 10.5 },
      { id: 2, name: 'Bier', price: 1.5 },
      { id: 3, name: 'Schokolade', price: 0.5 },
      { id: 4, name: 'Mais', price: 1.5 },
      { id: 5, name: 'Wasser', price: 1.5 },
      { id: 6, name: 'Cola', price: 10 },
      { id: 7, name: 'Erdnüsse', price: 10.4 },
      { id: 8, name: 'Hummer', price: 10.1 },
      { id: 9, name: 'Kaviar', price: 11 },
      { id: 10, name: 'Schinken', price: 15 },
      { id: 11, name: 'Brot', price: 10.5 },
      { id: 12, name: 'Bier', price: 1.5 },
      { id: 13, name: 'Schokolade', price: 0.5 },
      { id: 14, name: 'Mais', price: 1.5 },
      { id: 15, name: 'Wasser', price: 1.5 },
      { id: 16, name: 'Cola', price: 10 },
      { id: 17, name: 'Erdnüsse', price: 10.4 },
      { id: 18, name: 'Hummer', price: 10.1 },
      { id: 19, name: 'Kaviar', price: 11 },
      { id: 20, name: 'Schinken', price: 15 }
    ];
    return {products};
  }
}
