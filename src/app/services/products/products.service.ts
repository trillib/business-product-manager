import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../../models/product';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { ProductsServing } from 'app/services/products/products-interface';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable()
export class ProductsService implements ProductsServing {

  private productUrl = 'api/products';

  constructor(
    private http: HttpClient
  ) {}

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.productUrl)
      .pipe(
        catchError(this.handleError('getProducts', []))
      );
  }

  addProduct(product: Product): Observable<Product>{
    return this.http.post<Product>(this.productUrl, product, httpOptions)
  }

  deleteProduct(product: Product) {
    return this.http.delete<Product>(this.productUrl, httpOptions);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
