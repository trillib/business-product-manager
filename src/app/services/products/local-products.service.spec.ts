import { TestBed, inject } from '@angular/core/testing';

import { LocalProductsService } from './local-products.service';

describe('LocalProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalProductsService]
    });
  });

  it('should be created', inject([LocalProductsService], (service: LocalProductsService) => {
    expect(service).toBeTruthy();
  }));
});
