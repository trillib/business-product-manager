import { Observable } from 'rxjs/Observable';
import { Product } from 'app/models/product';

export interface ProductsServing {
  getProducts(): Observable<Product[]>;
  addProduct(product: Product): Observable<Product>;
  deleteProduct(product: Product);
}
