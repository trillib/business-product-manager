import { Injectable } from '@angular/core';
import { ProductsServing } from 'app/services/products/products-interface';
import { Observable } from 'rxjs/Observable';
import { Product } from 'app/models/product';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer } from 'electron';
import { ElectronHelper } from 'electron/electron-helper';

@Injectable()
export class LocalProductsService implements ProductsServing {

  ipcRenderer: typeof ipcRenderer;

  constructor() {
      this.ipcRenderer = window.require('electron').ipcRenderer;
  }

  getProducts(): Observable<Product[]> {
    this.ipcRenderer.send('receiveProducts');
    return new Observable<Array<Product>>(subscriber => {
      this.ipcRenderer.on('productsReceived', (event, result) => {
        subscriber.next(result);
      });
    });
  }

  addProduct(product: Product): Observable<Product> {
    this.ipcRenderer.send('saveProduct', product);
    return new Observable<Product>(subscriber => {
      this.ipcRenderer.on('productSaved', (event, productResult) => {
        subscriber.next(productResult);
      })
    });
  }

  deleteProduct(product: Product) {
    this.ipcRenderer.send('deleteProduct', product);
  }
}
