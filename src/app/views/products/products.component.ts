import { Component, OnInit, Injector, ChangeDetectorRef, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { ProductsService } from 'app/services/products/products.service';
import { Product } from 'app/models/product';
import { ProductsServing } from 'app/services/products/products-interface';
import { ElectronHelper } from 'electron/electron-helper';
import { LocalProductsService } from 'app/services/products/local-products.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {

  products: Product[];
  productsService: ProductsServing;

  constructor (
    private injector: Injector,
    private changeDet: ChangeDetectorRef,
  ) {
    if (ElectronHelper.isElectron()) {
      this.productsService = <LocalProductsService>this.injector.get(LocalProductsService);
    } else {
      this.productsService = <ProductsService>this.injector.get(ProductsService);
    }
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productsService.getProducts().subscribe(products => {
      this.products = products;
      this.changeDet.detectChanges();
    });
  }

  deleteProduct(product: Product) {
    swal({
      title: 'Bist du sicher?',
      text: 'Die Aktion ist nicht rückgängig',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ja löschen'
    }).then((result) => {
      if (result.value) {
        this.productsService.deleteProduct(product);
        this.products = this.products.filter(obj => obj !== product);
        this.changeDet.detectChanges();
        swal(
          'Gelöscht!',
          'Das Produkt wurde gelöscht.',
          'success'
        )
      }
    })
  }
}
