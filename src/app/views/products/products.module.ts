import { NgModule } from '@angular/core';

import { ProductsComponent } from './products.component';
import { ProductsRoutingModule } from './products-routing.module';
import { CommonModule } from '@angular/common';
import { ProductsService } from 'app/services/products/products.service';
import { LocalProductsService} from 'app/services/products/local-products.service'
import { ReactiveFormsModule } from '@angular/forms';
import { AddProductFormComponent } from './add-product-form/add-product-form.component';

@NgModule({
  imports: [
    ProductsRoutingModule,
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [ ProductsComponent, AddProductFormComponent ],
  providers: [
    ProductsService,
    LocalProductsService
  ]
})
export class ProductsModule { }
