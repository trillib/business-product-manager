import { Component, OnInit, ViewChild, Input, ElementRef, Injector, ChangeDetectorRef, NgZone } from '@angular/core';
import { Product } from 'app/models/product';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ElectronHelper } from 'electron/electron-helper';
import { ProductsServing } from 'app/services/products/products-interface';
import { LocalProductsService } from 'app/services/products/local-products.service';
import { ProductsService } from 'app/services/products/products.service';

@Component({
  selector: 'app-add-product-form',
  templateUrl: './add-product-form.component.html',
})
export class AddProductFormComponent implements OnInit {

  @Input() products: Product[];
  nameInputInvalid = false;
  priceInputInvalid = false;

  addForm = this.fb.group ({
    name: ['', Validators.required],
    price: ['', Validators.pattern(/^\d+$/)]
  });
  productsService: ProductsServing;


  constructor(
    private injector: Injector,
    private fb: FormBuilder,
    private zone: NgZone
  ) {
    if (ElectronHelper.isElectron()) {
      this.productsService = <LocalProductsService>this.injector.get(LocalProductsService);
    } else {
      this.productsService = <ProductsService>this.injector.get(ProductsService);
    }
  }

  ngOnInit() {
  }

  addProduct(event) {
    if (this.addForm.controls['name'].valid && this.addForm.controls['price'].valid) {
      const formValue = this.addForm.value;
      const product = {name: formValue.name, price: formValue.price} as Product;
      this.productsService.addProduct(product).subscribe(result => {
        this.zone.run(() => {
          this.addForm.reset();
          product.id = +result.id;
          this.products.push(product);
          this.nameInputInvalid = false;
          this.priceInputInvalid = false;
        });
      })
      } else if (!this.addForm.controls['name'].valid) {
          this.priceInputInvalid = false;
          this.nameInputInvalid = true;
      } else if (!this.addForm.controls['price'].valid) {
          this.nameInputInvalid = false;
          this.priceInputInvalid = true;
      }
  }
}
