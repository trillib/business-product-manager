import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2'

@Component({
  selector: 'app-sync-button',
  templateUrl: './sync-button.component.html',
  styleUrls: ['./sync-button.component.scss']
})
export class SyncButtonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  showSyncAlert() {
    swal({
      title: 'Daten synchronisieren?',
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#a5dc86',
      cancelButtonColor: '#949494',
      confirmButtonText: 'Jetzt Synchronisieren'
    }).then((result) => {
      if (result.value) {
        swal(
          'Synchronisiert',
          'Ihre Daten sind aktuell',
          'success'
        )
      }
    })
  }

}
