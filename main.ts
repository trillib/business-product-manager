import { app, BrowserWindow, screen, ipcMain } from 'electron';
import * as path from 'path';
import * as knex from 'knex';
import { Product } from './src/app/models/product';

let win

const personDB = knex({
  client: 'sqlite3',
  connection: {
    filename: path.join(__dirname, '../database/', 'db.sqlite')
  },
  useNullAsDefault: true
});

function createWindow() {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    x: 0,
    y: 0,
    width: size.width,
    height: size.height
  });

  // and load the index.html of the app.
  win.loadURL('file://' + __dirname + '/index.html');

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });
}

try {
  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', createWindow);

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

  ipcMain.on('receiveProducts', () => {
    const result = personDB.select(['id', 'name', 'price']).from('Product');
    result.then(rows => {
      const products = <Array<Product>> rows;
      win.webContents.send('productsReceived', products);
    });
  });

  ipcMain.on('saveProduct', (event, product: Product) => {
    personDB.insert(product).into('Product').then((id) => {
      product.id = id;
      win.webContents.send('productSaved', product);
    });
  })

  ipcMain.on('deleteProduct', (event, product: Product) => {
    console.log('in db');
    console.log(product);
    personDB('Product').where('id', product.id).del().then((e) => {
      console.log(e);
    });
  })

} catch (e) {
  // Catch Error
  // throw e;
}
